﻿using investprofile.dbcontext.Models;
using Microsoft.EntityFrameworkCore;

namespace investprofile.dbcontext
{
	public class InvestmentContext : DbContext
	{
		public DbSet<Portfolio> Portfolios { get; set; }
		public DbSet<SecurityPaper> SecurityPapers { get; set; }
		public DbSet<Operation> Operations { get; set; }
		public DbSet<Dividend> Dividends { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=invest_db;Username=postgres;Pooling=true;Timeout=15;");
		}
	}
}
