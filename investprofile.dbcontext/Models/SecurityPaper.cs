﻿using System.ComponentModel.DataAnnotations;

namespace investprofile.dbcontext.Models
{
	public class SecurityPaper
	{
		public int Id { get; set; }
		public int PortfolioId { get; set; }
		public Portfolio Portfolio { get; set; }
		[Required]
		public string Ticket { get; set; }
		[Required]
		public string Name { get; set; }
		public PaperType PaperType { get; set; }
		public float MeanPrice { get; set; }
		public int Count { get; set; }
	}
}
