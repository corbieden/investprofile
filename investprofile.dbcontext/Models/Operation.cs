﻿using System;

namespace investprofile.dbcontext.Models
{
	public class Operation
	{
		public int Id { get; set; }
		public int SecurityPaperId { get; set; }
		public SecurityPaper SecurityPaper { get; set; }
		public float Price { get; set; }
		public OperationType OperationType { get; set; }
		public DateTime Date { get; set; }
	}
}
