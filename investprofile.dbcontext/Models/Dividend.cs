﻿using System;

namespace investprofile.dbcontext.Models
{
	public class Dividend
	{
		public int Id { get; set; }
		public int SecurityPaperId { get; set; }
		public SecurityPaper SecurityPaper { get; set; }
		public DateTime Date { get; set; }
	}
}
