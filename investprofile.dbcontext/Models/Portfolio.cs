﻿using System;
using System.ComponentModel.DataAnnotations;

namespace investprofile.dbcontext.Models
{
	public class Portfolio
	{
		public int Id { get; set; }
		[Required]
		public string Name { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime? DeletedAt { get; set; }
	}
}
