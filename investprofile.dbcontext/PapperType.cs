﻿namespace investprofile.dbcontext
{
	public enum PaperType
	{
		Акция,
		Облигация,
		ETF,
		Рубль,
		Доллар,
		Евро
	}
}