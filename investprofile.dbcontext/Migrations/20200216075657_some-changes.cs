﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace investprofile.dbcontext.Migrations
{
    public partial class somechanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StartPrice",
                table: "SecurityPapers");

            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "SecurityPapers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PaperType",
                table: "SecurityPapers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Portfolios",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "Price",
                table: "Operations",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "SecurityPapers");

            migrationBuilder.DropColumn(
                name: "PaperType",
                table: "SecurityPapers");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Portfolios");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "Operations");

            migrationBuilder.AddColumn<float>(
                name: "StartPrice",
                table: "SecurityPapers",
                type: "real",
                nullable: false,
                defaultValue: 0f);
        }
    }
}
