﻿using System;

namespace investprofile.Models
{
	public class Dividend
	{
		public int Id { get; set; }
		public int SecurityPaperId { get; set; }
		public DateTime Date { get; set; }
	}
}
