﻿using investprofile.dbcontext;
using System;

namespace investprofile.Models
{
	public class Operation
	{
		public int Id { get; set; }
		public int SecurityPaperId { get; set; }
		public float Price { get; set; }
		public OperationType OperationType { get; set; }
		public DateTime Date { get; set; }
	}
}
