﻿using System;
using System.ComponentModel.DataAnnotations;

namespace investprofile.Models
{
	public class Portfolio
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime? DeletedAt { get; set; }
	}
}
