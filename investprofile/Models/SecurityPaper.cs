﻿using investprofile.dbcontext;

namespace investprofile.Models
{
	public class SecurityPaper
	{
		public int Id { get; set; }
		public int PortfolioId { get; set; }
		public Portfolio Portfolio { get; set; }
		public string Ticket { get; set; }
		public string Name { get; set; }
		public PaperType PaperType { get; set; }
		public int Count { get; set; }
		public float MeanPrice { get; set; }
		public float ActualPrice { get; set; }
	}
}
