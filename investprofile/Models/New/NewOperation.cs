﻿using System;
using investprofile.dbcontext;

namespace investprofile.Models.New
{
	public class NewOperation
	{
		public int PortfolioId { get; set; }
		public string Ticket { get; set; }
		public float Price { get; set; }
		public OperationType OperationType { get; set; }
		public DateTime Date { get; set; }
	}
}
