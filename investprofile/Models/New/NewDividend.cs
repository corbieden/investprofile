﻿using System;

namespace investprofile.Models.New
{
	public class NewDividend
	{
		public int SecurityPaperId { get; set; }
		public DateTime Date { get; set; }
	}
}
