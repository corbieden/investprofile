﻿using System.Net.Http;
using System.Threading.Tasks;

namespace investprofile.Services
{
	public class MoexApiService : IMoexApiService
	{
		private readonly string _Api = "https://iss.moex.com/iss";
		public async Task<string> GetSecurityInfoByTicket(string ticket)
		{
			var link = $"{_Api}/securities.json?q={ticket}";
			using (var client = new HttpClient())
			{
				return await client.GetStringAsync(link);
			}
		}
	}

	public interface IMoexApiService
	{
		Task<string> GetSecurityInfoByTicket(string ticket);
	}
}