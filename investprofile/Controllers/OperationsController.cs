﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using investprofile.dbcontext;
using investprofile.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using investprofile.Models.New;
using investprofile.Services;

namespace investprofile.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class OperationsController : ControllerBase
	{
		private readonly InvestmentContext _investmentContext;
		private readonly IMapper _mapper;
		private readonly IMoexApiService _moexApiService;

		public OperationsController(InvestmentContext investmentContext, IMapper mapper, IMoexApiService moexApiService)
		{
			_investmentContext=investmentContext;
			_mapper=mapper;
			_moexApiService = moexApiService;
		}

		[HttpGet("getAll")]
		public async Task<IEnumerable<Operation>> GetOperations()
		{
			var operations = await _investmentContext.Operations
				.ProjectTo<Operation>(_mapper.ConfigurationProvider)
				.ToListAsync();

			return operations;
		}

		[HttpPost("add")]
		public async Task<int> AddNewOperation([FromBody] NewOperation newOperation, [FromQuery] int count = 1)
		{
			// check SP with this ticket and update mean price
			var paper = await _investmentContext.SecurityPapers
				.FirstOrDefaultAsync(__paper => __paper.Ticket==newOperation.Ticket);

			if (paper == null)
			{
				var xml = await _moexApiService.GetSecurityInfoByTicket(newOperation.Ticket);

				var xDoc = new XmlDocument();
				xDoc.LoadXml(xml);

				var xNodes = xDoc.DocumentElement.ChildNodes[0].ChildNodes[1].ChildNodes;

				foreach (XmlElement xnode in xNodes)
				{
					var name = xnode.Attributes.GetNamedItem("secid").Value;
					if (name == newOperation.Ticket)
					{
						var newPaper = new SecurityPaper()
						{
							ActualPrice=newOperation.Price,
							MeanPrice=newOperation.Price,
							Count=count,
							Ticket=newOperation.Ticket,
							PortfolioId=newOperation.PortfolioId
						};
					}
				}

				
			}

			var newMeanPrice = CalculateMeanPrice(
				paper.Count,
				paper.MeanPrice,
				count,
				newOperation.Price
				);

			paper.MeanPrice=newMeanPrice;
			paper.Count+=count;

			await _investmentContext.SaveChangesAsync();

			return 15;
		}

		private float CalculateMeanPrice(int oldCount, float oldMeanPrice, int addCount, float addPrice)
		{
			return (oldCount*oldMeanPrice+addCount*addPrice)/(oldCount+addCount);
		}
	}
}