﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using investprofile.dbcontext;
using investprofile.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace investprofile.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SecurityPapersController : ControllerBase
	{
		private readonly InvestmentContext _investmentContext;
		private readonly IMapper _mapper;

		public SecurityPapersController(InvestmentContext investmentContext, IMapper mapper)
		{
			_investmentContext=investmentContext;
			_mapper=mapper;
		}

		[HttpGet("get")]
		public async Task<IEnumerable<SecurityPaper>> GetSecurityPapersById([FromQuery] int portfolioId)
		{
			var pappers = await _investmentContext.SecurityPapers
				.ProjectTo<SecurityPaper>(_mapper.ConfigurationProvider)
				.Where(__papper => __papper.PortfolioId==portfolioId)
				.ToListAsync();

			return pappers;
		}

		private float CalculateMeanPrice(int oldCount, float oldMeanPrice, int addCount, float addPrice)
		{
			return (oldCount*oldMeanPrice+addCount*addPrice)/(oldCount+addCount);
		}
	}
}