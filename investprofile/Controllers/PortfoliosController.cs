﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using investprofile.dbcontext;
using investprofile.Models;
using investprofile.Models.New;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace investprofile.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class PortfoliosController : ControllerBase
	{
		private readonly InvestmentContext _investmentContext;
		private readonly IMapper _mapper;

		public PortfoliosController(InvestmentContext investmentContext, IMapper mapper)
		{
			_investmentContext=investmentContext;
			_mapper=mapper;
		}

		[HttpGet("getAll")]
		public async Task<IEnumerable<Portfolio>> GetAll()
		{
			var portfolios = await _investmentContext.Portfolios
				.ProjectTo<Portfolio>(_mapper.ConfigurationProvider)
				.ToListAsync();

			return portfolios;
		}

		[HttpPost("add")]
		public async Task<int> AddNewPortfolio([FromBody] NewPortfolio newPortfolio)
		{
			var isExists =
				await _investmentContext.Portfolios
					.AnyAsync(__portfolio => __portfolio.Name==newPortfolio.Name);

			if (isExists)
				throw new Exception($"Portfolio with name {newPortfolio.Name} already exists");

			var mappedPortfolio = _mapper.Map<dbcontext.Models.Portfolio>(newPortfolio);
			mappedPortfolio.CreatedAt=DateTime.Now;
			var portfolio = await _investmentContext.Portfolios.AddAsync(mappedPortfolio);

			await _investmentContext.SaveChangesAsync();
			return portfolio.Entity.Id;
		}

		[HttpDelete("delete")]
		public async Task SoftDelete(int id)
		{
			var portfolioToDelete = await _investmentContext.Portfolios
				.FirstOrDefaultAsync(__portfolio => __portfolio.Id==id);

			if (portfolioToDelete==null)
				throw new KeyNotFoundException($"Portfolio with id {id} not found");

			portfolioToDelete.DeletedAt=DateTime.Now;
			await _investmentContext.SaveChangesAsync();
		}
	}
}