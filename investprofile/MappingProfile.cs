﻿using AutoMapper;

namespace investprofile
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<Models.Dividend, dbcontext.Models.Dividend>();
			CreateMap<Models.Operation, dbcontext.Models.Operation>();
			CreateMap<Models.Portfolio, dbcontext.Models.Portfolio>();
			CreateMap<Models.SecurityPaper, dbcontext.Models.SecurityPaper>();
			CreateMap<dbcontext.Models.Dividend, Models.Dividend>();
			CreateMap<dbcontext.Models.Operation, Models.Operation>();
			CreateMap<dbcontext.Models.Portfolio, Models.Portfolio>();
			CreateMap<dbcontext.Models.SecurityPaper, Models.SecurityPaper>();
		}
	}
}
